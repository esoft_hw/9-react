import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './index.css';
import App from './App';
// import reportWebVitals from './reportWebVitals';
import GameField from './pages/GameField/GameField';
import Auth from './pages/Auth/Auth';
import Rating from './pages/Rating/Rating';
import ActivePlayers from './pages/ActivePlayers/ActivePlayers';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<GameField />} />
          <Route path="/auth" element={<Auth />} />
          <Route path="/rating" element={<Rating />} />
          <Route path="/activePlayers" element={<ActivePlayers />} />
        </Routes>
    </BrowserRouter>,
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals(); ВЕРНУТЬ!!!!
