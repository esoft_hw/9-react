import React from "react";
import './freePlayer.css'

function FreePlayer({className}) {
  return (
    <div className={`free ${className}`}>
      <p>Свободен</p> 
    </div>
  );
}

export default FreePlayer;
