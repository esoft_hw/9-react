import React from "react";
import './blockedPlayer.css'

function BlockedPlayer({className}) {
  return (
    <div className={`blocked ${className}`}>
      <p>Заблокирован</p> 
    </div>
  );
}

export default BlockedPlayer;