import React from "react";
import './outGamePlayer.css'

function OutGamePlayer({className}) {
  return (
    <div className={`outGame ${className}`}>
      <p>Вне игры</p> 
    </div>
  );
}

export default OutGamePlayer;