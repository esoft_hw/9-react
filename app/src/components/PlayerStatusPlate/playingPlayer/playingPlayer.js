import React from "react";
import './playingPlayer.css'

function PlayingPlayer({className}) {
  return (
    <div className={`playing ${className}`}>
      <p>В игре</p> 
    </div>
  );
}

export default PlayingPlayer;