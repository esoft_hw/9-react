import React, { useState } from "react";
import bcrypt from "bcryptjs";
import Input from '../Inputs/input'
import './AuthForm.css';

import dogSvg from '../../assets/svg/login-dog.svg';

const users = [
  { login: "user1@bk.ru", password: "$2y$10$xOEv5t06/biJVIEbsYar7uX7dzJ4nyRjRe8SRVuwJmO9KnZc/E3UG" }, // пароль: password1
  { login: "user2@ya.ru", password: "$2y$10$kf8GlzXuuJ0/tRK4CLzqzO12CoOD4RkmgWwZZArScDEaYpFZPjPLK" }, // пароль: password2
];

let ErrorTabLogin = "";
let ErrorTabPassword = "";
let ErrorLogin = "";
let ErrorPassword = "";
let gap10 = "";

const AuthForm = () => {
  const [loginValue, setLoginValue] = useState("");
  const [passwordValue, setPasswordValue] = useState("");
  const forValidTestLogin = /^[a-zA-Z0-9._@]+$/;
  const forValidTestPassword = /^[a-zA-Z0-9._]+$/;
  const bcrypt = require('bcryptjs');

  // Проверка на валидность
  const handleLoginChange = (event) => {
    setLoginValue(event.target.value.trim());
  };

  const handlePasswordChange = (event) => {
    setPasswordValue(event.target.value.trim());
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    let isValidLogin = forValidTestLogin.test(loginValue);
    let isValidPassword = forValidTestPassword.test(passwordValue);

    if (!isValidLogin) {
      console.error("Невалидный логин!");
      ErrorTabLogin = "error";
      ErrorLogin = "loginError";
      gap10 = "gap10";
      return;
    } else if (!isValidPassword) {
      console.error("Невалидный пароль!");
      ErrorTabPassword = "error";
      ErrorPassword = "passwordError";
      gap10 = "gap10";
      return;
    } else {
      console.log(`Логин: ${loginValue} , пароль: ${passwordValue}`);
    }

    // Поиск пользователя + проверка пароля 
    const user = users.find((user) => user.login === loginValue);
    if (!user) {
      console.error("Пользователь не найден");
      ErrorTabLogin = "error";
      ErrorLogin = "loginError";
      gap10 = "gap10";
      setLoginValue("");
      setPasswordValue("");
      return;
    }

    bcrypt.compare(passwordValue, user.password, function(err, result) {
      if (result) {
        window.location.href = "/rating";
      }
      else {
        console.error("Неверный пароль");
        ErrorTabPassword = "error";
        ErrorPassword = "passwordError";
        gap10 = "gap10";
        setPasswordValue("");
        return;
      }
  });

  };
    return (
        <main class="aut-login">
            <form class={`login-window ${gap10}`} onSubmit={handleSubmit}>
                <img src={dogSvg} alt="Пёс" class="login-img"/>
                <p class="login-text">Войдите в игру</p>
                <Input type="email" id="login" placeholder="Логин" className={ErrorTabLogin} value={loginValue} onChange={handleLoginChange}/>
                <p className={`invisible ${ErrorLogin}`}>Неверный логин</p>
                <Input type="password" id="password" placeholder="Пароль" className={ErrorTabPassword} value={passwordValue} onChange={handlePasswordChange}/>
                <p className={`invisible ${ErrorPassword}`}>Неверный пароль</p>
                <button type="submit" onclick="SubmitForm()" class="send-login">Войти</button>
            </form>
        </main>
    );
  };

export default AuthForm;