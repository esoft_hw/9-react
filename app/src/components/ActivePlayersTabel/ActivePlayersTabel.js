import React from "react";
import './ActivePlayersTabel.css';

import FreePlayer from "../PlayerStatusPlate/freePlayer/freePlayer";
import Input from "../Inputs/input";
import PlayingPlayer from "../PlayerStatusPlate/playingPlayer/playingPlayer";
import GreenButton from "../Buttuns/GreenBtn/GreenBtn";
import GrayButton from "../Buttuns/GrayBtn/GrayBtn";
import Switch from "../Switch/Switch";

function ActivePlayersTabel() {
    return (
        <main className="page-active">
        <table className="active-tabel">
            <thead>
                <tr className="active-tabel__headRow">
                    <td><h1 className="title-active">Активные игроки</h1></td>
                    <th><Switch text={"Только свободные"}/></th>
                </tr>
            </thead>
            <tbody className="active-tabel__body">
                <tr className="active-row"> 
                    <td className="col1"><p>Александров Игнат Анатолиевич</p></td>
                    <div className="flex">
                        <td className="col2"><FreePlayer className="plateSize"/></td>
                        <td className="col3"><GreenButton text={"Позвать играть"} className={"tabel-btn"}/></td>
                    </div>
                </tr>
                <tr className="active-row"> 
                    <td className="col1"><p>Александров Игнат Анатолиевич</p></td>
                    <div className="flex">
                        <td className="col2"><FreePlayer className="plateSize"/></td>
                        <td className="col3"><GreenButton text={"Позвать играть"} className={"tabel-btn"}/></td>
                    </div>
                </tr>
                <tr className="active-row"> 
                    <td className="col1"><p>Александров Игнат Анатолиевич</p></td>
                    <div className="flex">
                        <td className="col2"><PlayingPlayer className="plateSize"/></td>
                        <td className="col3"><GrayButton text={"Позвать играть"} className={"tabel-btn"}/></td>
                    </div>
                </tr>
                <tr className="active-row"> 
                    <td className="col1"><p>Александров Игнат Анатолиевич</p></td>
                    <div className="flex">
                        <td className="col2"><FreePlayer className="plateSize"/></td>
                        <td className="col3"><GreenButton text={"Позвать играть"} className={"tabel-btn"}/></td>
                    </div>
                </tr>
                <tr className="active-row"> 
                    <td className="col1"><p>Александров Игнат Анатолиевич</p></td>
                    <div className="flex">
                        <td className="col2"><PlayingPlayer className="plateSize"/></td>
                        <td className="col3"><GrayButton text={"Позвать играть"} className={"tabel-btn"}/></td>
                    </div>
                </tr>
                <tr className="active-row"> 
                    <td className="col1"><p>Александров Игнат Анатолиевич</p></td>
                    <div className="flex">
                        <td className="col2"><PlayingPlayer className="plateSize"/></td>
                        <td className="col3"><GrayButton text={"Позвать играть"} className={"tabel-btn"}/></td>
                    </div>
                </tr>
                <tr className="active-row"> 
                    <td className="col1"><p>Александров Игнат Анатолиевич</p></td>
                    <div className="flex">
                        <td className="col2"><FreePlayer className="plateSize"/></td>
                        <td className="col3"><GreenButton text={"Позвать играть"} className={"tabel-btn"}/></td>
                    </div>
                </tr>
            </tbody>        
        </table>
    </main>
    );
}

export default ActivePlayersTabel