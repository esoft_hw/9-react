import React, { useEffect, useState } from "react";

const Delayed = ({ children, waitBeforeShow }) => {
  const [isShown, setIsShown] = useState(false);

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setIsShown(true);
    }, waitBeforeShow);

    return () => {
      clearTimeout(timeoutId);
    };
  }, [waitBeforeShow]);

  return isShown ? <>{children}</> : null;
};

export default Delayed;
