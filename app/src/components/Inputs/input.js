import React from "react";
import './input.css'

const Input = ({ type, id, placeholder, value, onChange, className }) => {
  return (
    <input
      type={type}
      id={id}
      className={`input_message ${className}`}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
    />
  );
};

export default Input;