import React from 'react';
import './Header.css'
import { Link, useLocation } from 'react-router-dom';

import logoSvg from '../../assets/svg/logo.svg';
import authSvg from '../../assets/svg/icon-button.svg';


function Header() {
    const location = useLocation();

    return (
        <header class="header">
        <nav class="navbar">
            <Link to = "/" class="logo">
                <img src={logoSvg} alt="Логотип сайта XO"/>
            </Link>
            <div class="nav_list">
                <ui class="nav_list__item"><Link to = "/" className={location.pathname === '/' ? 'active' : ''}>Игровое поле</Link></ui>
                <ui class="nav_list__item"><Link to="/rating" className={location.pathname === '/rating' ? 'active' : ''}>Рейтинг</Link></ui>
                <ui class="nav_list__item"><Link to="/activePlayers" className={location.pathname === '/activePlayers' ? 'active' : ''}>Активные игроки</Link></ui>
                <ui class="nav_list__item">История игр</ui>
                <ui class="nav_list__item">Список игроков</ui>
            </div>
            <Link to="/auth" class="login">
                <img src={authSvg} alt="Вход в аккаунт"/>
            </Link>
        </nav>
</header>

  );
}

export default Header;