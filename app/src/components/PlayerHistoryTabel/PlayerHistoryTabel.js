import React from "react";
import './RatingTable.css';

function RatingTable() {
    return (
        <main class="page-rating">
        <table class="page-rating__tabel">
            <thead class="thead-rating">
                <tr>
                    <td><h1 class="name-rating-tabel">Рейтинг игроков</h1></td>
                </tr>
                <tr class="first-dis-tabel">
                    <th><p class="FIO-tabel marg-8">ФИО</p></th>
                    <th><p class="all-game-tabel">Всего игр</p></th>
                    <th><p class="all-wins-tabel">Победы</p></th>
                    <th><p class="all-losses-tabel">Проигрыши</p></th>
                    <th><p class="percent-wins">Процент побед</p></th>
                </tr>
            </thead>
            <tbody class="tbody-rating">
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
                <tr class="first-dis-tabel"> 
                    <td><p class="FIO-tabel marg-8">Александров Игнат Анатолиевич</p></td>
                    <td><p class="all-game-tabel">24534</p></td>
                    <td><p class="color_green-table all-wins-tabel">9824</p></td>
                    <td><p class="color_red-table all-losses-tabel">1222</p></td>
                    <td><p class="percent-wins">87%</p></td>
                </tr>
            </tbody>        
        </table>
    </main>
    );
}

export default RatingTable