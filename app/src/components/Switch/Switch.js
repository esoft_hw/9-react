import React, { useState } from "react";
import "./Switch.css";

function Switch({text}) {
  const [checked, setChecked] = useState(false);

  const handleToggle = () => {
    setChecked(!checked);
  };

  return (
    <div className="switch">
      <input
        type="checkbox"
        name="switch1"
        id="switch1"
        checked={checked}
        onChange={handleToggle}
      />
      <label htmlFor="switch1" className="switch__label">
        {text}
      </label>
    </div>
  );
}

export default Switch;