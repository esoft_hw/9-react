import React from 'react';

function ButtonBasic({ onClick, className, text }) {
  return (
    <button onClick={onClick} className={className}>{text}</button>
  );
}

export default ButtonBasic;