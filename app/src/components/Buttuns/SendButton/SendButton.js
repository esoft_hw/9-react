import React from "react";
import './SendButton.css'

import sendSvg from '../../../assets/svg/send.svg'

function SendButton() {
  return (
    <button className="btn_send_message">
      <img src={sendSvg} alt="Отправить письмо" />
    </button>
  );
}

export default SendButton;