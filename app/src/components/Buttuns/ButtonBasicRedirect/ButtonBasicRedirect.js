import React, { useState } from 'react';
import { Navigate } from 'react-router-dom';

function ButtonBasicRedirect({ pageLink, className, text }) {
  const [redirect, setRedirect] = useState(false);

  const handleClick = () => {
    setRedirect(true);
  };

  if (redirect) {
    return <Navigate to={pageLink} />;
  }

  return <button onClick={handleClick} className={className}>{text}</button>;
}

export default ButtonBasicRedirect;