import React from 'react';
import './GrayBtn.css'

function GrayButton({ onClick, className, text }) {
  return (
    <button onClick={onClick} className={`gray_btn ${className}`}>{text}</button>
  );
}

export default GrayButton;