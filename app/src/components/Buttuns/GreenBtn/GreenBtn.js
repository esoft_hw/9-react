import React from 'react';
import './GreenBtn.css'

function GreenButton({ onClick, className, text }) {
  return (
    <button onClick={onClick} className={`green_btn ${className}`}>{text}</button>
  );
}

export default GreenButton;