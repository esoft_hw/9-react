import React from "react";
import Message1 from "../Message1/Message1";
import Message2 from "../Message2/Message2";
import Input from "../../Inputs/input";
import Button from "../../Buttuns/SendButton/SendButton";
import './Chat.css'

function Chat() {
  return (
    <div className="chat">
      <Message1 />
      <Message2 />
      <Message2 />
      <Message1 />
      <div className="send_message">
        <Input type="text" placeholder="Сообщение..." onCopy="true"/>
        <Button />
      </div>
    </div>
  );
}

export default Chat;