import React from "react";
import './Field.css'

const Cell = ({ value, onClick, className }) => (
  <div className={`field-item ${className}`} onClick={onClick}>
    {value}
  </div>
);

export default Cell;