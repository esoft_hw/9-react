import React, { Component } from "react";
import Cell from "./Cell";
import "./Field.css";
import WinnerModal from "../WinnerModal/WinnerModal";
import MoveNow from "../MoveNow/MoveNow";

import X from '../../../assets/svg/X.svg';
import O from '../../../assets/svg/O.svg';
import Score from "../Score/Score";
import Delayed from "../../Delayed";


class GameField extends Component {
  constructor(props) {
    super(props);

    this.state = {
      board: Array(9).fill(null),
      currentPlayer: "X",
      winner: null,
    };
  }

  handleClick = (i) => {
    const board = this.state.board.slice();
    if (this.state.winner || board[i]) {
      return;
    }
    board[i] = this.state.currentPlayer;
    const winner = this.calculateWinner(board);
    const currentPlayer = this.state.currentPlayer === "X" ? "O" : "X";
    this.setState({
      board: board,
      currentPlayer: currentPlayer,
      winner: winner,
    });
  };

  calculateWinner = (board) => {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (board[a] && board[a] === board[b] && board[a] === board[c]) {
        return board[a];
      }
    }
    return null;
  };

  resetGame = () => {
    this.setState({
      board: Array(9).fill(null),
      currentPlayer: "X",
      winner: null,
    });
  };

  renderCell = (i) => {
    const value = this.state.board[i];
    let imageSrc = '';
    if (value === 'X') {
      imageSrc = X;
    } else if (value === 'O') {
      imageSrc = O;
    }

  
    return <Cell value={<img src={imageSrc} alt={value} />} onClick={() => this.handleClick(i)} />;
  };


  render() {
    const winner = this.state.winner;
    let status;
    if (winner) {
      status = "Winner: " + winner;
    } else if (this.state.board.every((cell) => cell !== null)) {
      status = "Draw!";
    } else {
      status = "Next player: " + this.state.currentPlayer;
    }

    return (
      <main className="main">
        <Score />
        <div className="field"> 
            {this.renderCell(0)}
            {this.renderCell(1)}
            {this.renderCell(2)}        
            {this.renderCell(3)}
            {this.renderCell(4)}
            {this.renderCell(5)}
            {this.renderCell(6)}
            {this.renderCell(7)}
            {this.renderCell(8)}
        </div>
        <MoveNow currentPlayer={this.state.currentPlayer} />
        {winner && (
        <Delayed waitBeforeShow={500}>
          <WinnerModal winner={winner} onReset={this.resetGame} />
        </Delayed>
        ) || (this.state.board.every((cell) => cell !== null)) && <Delayed waitBeforeShow={500}>
        <WinnerModal winner={"Ничья!"} onReset={this.resetGame} />
      </Delayed>
        }

      </main>
    );
  }
}

export default GameField;