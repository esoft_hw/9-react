import React, { useState } from 'react';

function WalkXO() {
  const [field, setField] = useState(Array(3).fill().map(() => Array(3).fill(null)));
  const [whoWalks, setWhoWalks] = useState('x');
  const [isOverGame, setIsOverGame] = useState(false);

  const checkField = () => {
    for (let i = 0; i < 3; i++) {
      if (field[i][0] === field[i][1] && field[i][1] === field[i][2] && field[i][1] !== null) {
        return field[i][0];
      }
      if (field[0][i] === field[1][i] && field[1][i] === field[2][i] && field[1][i] !== null) {
        return field[0][i];
      }
    }
    if (field[0][0] === field[1][1] && field[1][1] === field[2][2] && field[1][1] !== null) {
      return field[0][0];
    }
    if (field[0][2] === field[1][1] && field[1][1] === field[2][0] && field[1][1] !== null) {
      return field[0][2];
    }
    return '-';
  };

  const checkFreeCell = (x, y) => {
    return field[x][y] === null;
  };

  const noMoves = () => {
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        if (field[i][j] === null) {
          return false;
        }
      }
    }
    return true;
  };

  const FieldCellValue = (x, y) => {
    if (!checkFreeCell(x, y)) {
      alert('Это поле уже занято! Введите другое');
      return;
    }

    const newField = [...field];
    newField[x][y] = whoWalks;
    setField(newField);

    if (whoWalks === 'x') {
      setWhoWalks('o');
    } else {
      setWhoWalks('x');
    }
  };

  const printResult = (value) => {
    if (value === 'x') {
      alert('Крестики победили\n');
      setIsOverGame(true);
    } else if (value === 'o') {
      alert('Нолики победили\n');
      setIsOverGame(true);
    } else if (value === '-') {
      alert('Ничья\n');
      setIsOverGame(true);
    }
  };

  const findWiner = () => {
    printResult(checkField());
  };

  const onCellClick = (x, y) => {
    if (isOverGame || !checkFreeCell(x, y)) {
      return;
    }

    FieldCellValue(x, y);

    if (noMoves() || checkField() !== '-') {
      findWiner();
    }
  };

  function TicTacToe() {
    const [gameField, setGameField] = useState([
      [null, null, null],
      [null, null, null],
      [null, null, null]
    ]);
    const [activePlayer, setActivePlayer] = useState('x');
    const [isOverGame, setIsOverGame] = useState(false);
  
    function onCellClick(rowIndex, cellIndex) {
      if (gameField[rowIndex][cellIndex] !== null || isOverGame) {
        return;
      }
  
      const updatedField = [...gameField];
      updatedField[rowIndex][cellIndex] = activePlayer;
      setGameField(updatedField);
      setActivePlayer(activePlayer === 'x' ? 'o' : 'x');
  
      if (noMoves() || winnerIsFound()) {
        setIsOverGame(true);
      }
    }
  
    function noMoves() {
      for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
          if (gameField[i][j] === null) {
            return false;
          }
        }
      }
      return true;
    }
  
    function winnerIsFound() {
      for (let i = 0; i < 3; i++) {
        if (
          gameField[i][0] === gameField[i][1] &&
          gameField[i][1] === gameField[i][2] &&
          gameField[i][0] !== null
        ) {
          return true;
        } else if (
          gameField[0][i] === gameField[1][i] &&
          gameField[1][i] === gameField[2][i] &&
          gameField[0][i] !== null
        ) {
          return true;
        }
      }
      if (
        gameField[0][0] === gameField[1][1] &&
        gameField[1][1] === gameField[2][2] &&
        gameField[0][0] !== null
      ) {
        return true;
      } else if (
        gameField[0][2] === gameField[1][1] &&
        gameField[1][1] === gameField[2][0] &&
        gameField[0][2] !== null
      ) {
        return true;
      }
      return false;
    }
  
    function renderCell(rowIndex, cellIndex) {
      const cellValue = gameField[rowIndex][cellIndex];
      return (
        <td key={cellIndex} onClick={() => onCellClick(rowIndex, cellIndex)}>
          {cellValue === 'x' && <img src="./svg/x-play.svg" alt="X" />}
          {cellValue === 'o' && <img src="./svg/zero-play.svg" alt="O" />}
        </td>
      );
    }
  
    return (

    <div>
        <table>
          <tbody>
            {gameField.map((row, rowIndex) => (
              <tr key={rowIndex}>
                {row.map((cell, cellIndex) => renderCell(rowIndex, cellIndex))}
                </tr>
            ))}
            </tbody>
        </table>
    </div>
);}}

export default WalkXO;