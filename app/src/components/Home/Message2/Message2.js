import React from "react";
import "./Message2.css"

function Message2() {
  return (
    <div className="message_2">
      <div className="message_player">
        <p className="name color_pink">Пупкин Владлен</p>
        <p className="time">13:41</p>
      </div>
      <p className="message_text">Надо было играть за крестики. Розовый — мой не самый счастливый цвет</p>
    </div>
  );
}

export default Message2;