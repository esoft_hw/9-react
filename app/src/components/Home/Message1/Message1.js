import React from "react";
import "./Message1.css"

function Message1() {
  return (
    <div className="message_1">
      <div className="message_player">
        <p className="name color_green">Плюшкина Екатерина</p>
        <p className="time">13:40</p>
      </div>
      <p className="message_text">Ну что, готовься к поражению!!1</p>
    </div>
  );
}

export default Message1;