import React from "react";
import './Players.css'

import OSvg from '../../../assets/svg/O.svg'
import XSvg from '../../../assets/svg/X.svg'

function Players() {
  return (
    // <aside className="aside">
      <div className="players">
        <p className="players_name">Игроки</p>
        <div className="player">
          <img src={OSvg} alt="O" className="figure" />
          <div className="player_property">
            <p className="player_name">Пупкин Владлен Игоревич</p>
            <p className="win_percent">63% побед</p>
          </div>
        </div>
        <div className="player">
          <img src={XSvg} alt="X" className="figure" />
          <div className="player_property">
            <p className="player_name">Плюшкина Екатерина Викторовна</p>
            <p className="win_percent">23% побед</p>
          </div>
        </div>
      </div>
    // </aside>
  );
}

export default Players;