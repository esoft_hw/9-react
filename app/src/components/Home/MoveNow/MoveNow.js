import React from "react";
import "./MoveNow.css";
import XSvg from "../../../assets/svg/X.svg";
import OSvg from "../../../assets/svg/O.svg";

const MoveNow = ({ currentPlayer }) => {
  const imgSrc = currentPlayer === "X" ? XSvg : OSvg;

  return (
    <div className="move_now">
      <p className="move">Ходит</p>
      <img src={imgSrc} alt="Сейчас ходит" className="figure" id="xo_player_tabel" />
      <p className="player_move" id="current_player">Владлен Пупкин</p>
    </div>
  );
};

export default MoveNow;
