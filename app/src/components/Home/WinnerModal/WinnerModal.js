import React, { Component } from "react";
import './WinnerModal.css'
import cup from '../../../assets/svg/cup.svg'
import { redirect } from "react-router-dom";
import ButtonBasic from "../../Buttuns/ButtonBasic/ButtonBasic";
import ButtonBasicRedirect from "../../Buttuns/ButtonBasicRedirect/ButtonBasicRedirect";



class WinnerModal extends Component {
  render() {
    const { winner, onReset } = this.props;
    return (
      <div className="modal__overlay">
        <div className="modal">
        <img src={cup} className="modal_cup"/>
        <p className="modal_text">Владлен Пупкин победил!{winner}</p>
        < ButtonBasic onClick={onReset} className={"btn_newGame"} text={"Новая игра"} />
        < ButtonBasicRedirect pageLink={"/rating"} className={"btn_menu"} text={"Выйти в меню"} />
      </div>
      </div>

    );
  }
}

export default WinnerModal;