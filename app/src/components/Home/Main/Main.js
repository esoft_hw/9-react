import React from 'react';
import Players from '../Players/Players';
import Score from '../Score/Score';
import Field from '../Field/Field';
import MoveNow from '../MoveNow/MoveNow';
import Chat from '../Chat/Chat';

import './Main.css';
import GameField from '../Field/Field';

function Main() {
  return (
    <div className="main_container">
      <aside className="aside">
        <Players />
      </aside>
      <GameField />
      <aside className="aside-2">
        <Chat />
      </aside>
    </div>
  );
}

export default Main;