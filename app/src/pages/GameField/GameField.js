import React from "react";
import './GameField.css'
import Header from "../../components/Header/Header";
import Footer from '../../components/Footer/Footer';
import Main from '../../components/Home/Main/Main';

function GameField() {
    return (
        <div class="container">
        <Header />
        <Main />
        <Footer />
        </div>
    );
}

export default GameField