import React from "react";
import './Rating.css'
import Header from "../../components/Header/Header";
import Footer from '../../components/Footer/Footer';
import RatingTable from "../../components/RatingTable/RatingTable";

function Rating() {
    return (
        <div class="container">
        <Header />
        <RatingTable />
        <Footer />
        </div>
    );
}

export default Rating