import React from "react";
import './ActivePlayers.css'
import Header from "../../components/Header/Header";
import Footer from '../../components/Footer/Footer';
import ActivePlayersTabel from "../../components/ActivePlayersTabel/ActivePlayersTabel";

function ActivePlayers() {
    
    return (
        <div class="container">
        <Header />
        <ActivePlayersTabel />
        <Footer />
        </div>
    );
}

export default ActivePlayers