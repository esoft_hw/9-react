import React from "react";
import './Auth.css'
import Header from "../../components/Header/Header";
import Footer from '../../components/Footer/Footer';
import AuthForm from "../../components/AuthForm/AuthForm";

function Auth() {
    
    return (
        <div class="container">
        <Header />
        <AuthForm />
        <Footer />
        </div>
    );
}

export default Auth